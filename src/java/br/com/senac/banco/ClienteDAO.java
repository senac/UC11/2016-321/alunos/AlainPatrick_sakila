
package br.com.senac.banco;

import br.com.senac.modelo.Cidade;
import br.com.senac.modelo.Cliente;
import br.com.senac.modelo.Endereco;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ClienteDAO implements DAO<Cliente> {

    @Override
    public void salvar(Cliente objeto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void atualizar(Cliente objeto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deletar(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Cliente> listarTodos() {
       
        List<Cliente> lista = new ArrayList<>();
        
        Connection connection = null;
        
         try {
            connection = Conexao.getConnection();

            String query = "SELECT * FROM customer cu inner join address a on cu.address_ID = a.address_ID ; ";

            Statement st = connection.createStatement();

            ResultSet rs = st.executeQuery(query);

            while (rs.next()) {

                Endereco endereco = new Endereco(rs.getInt("address_ID"), rs.getString("address"));

                Cliente cliente = new Cliente(rs.getInt("customer_ID"), rs.getString("customer"), endereco);

                lista.add(cliente);
            }

        } catch (SQLException ex) {
            System.err.println("Erro ao atualizar ....");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar o banco");
            }
        }

        return lista;
    }

    @Override
    public Cliente buscarPorId(int id) {

        Connection connection = null;
        Cliente cliente = null ; 

        try {
            connection = Conexao.getConnection();

            String query = "SELECT * FROM customer cu inner join address a "
                    + "on cu.address_ID = a.address_ID where address_ID = ?  ; ";

            PreparedStatement ps = connection.prepareStatement(query);

            ResultSet rs = ps.executeQuery();

            if (rs.first()) {
               
                Endereco endereco = new Endereco(rs.getInt("address_ID"), rs.getString("address"));

                cliente = new Cliente(rs.getInt("customer_ID"), rs.getString("customer"), endereco);
            }  
            

        } catch (SQLException ex) {
            System.err.println("Erro ao atualizar ....");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar o banco");
            }
        }
        
        return cliente;

    }
}
