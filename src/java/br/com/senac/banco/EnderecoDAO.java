/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.banco;

import br.com.senac.modelo.Cidade;
import br.com.senac.modelo.Endereco;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Roberto
 */
public class EnderecoDAO implements DAO<Endereco> {

    @Override
    public void salvar(Endereco objeto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void atualizar(Endereco objeto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deletar(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Endereco> listarTodos() {
      List<Endereco> lista = new ArrayList<>();

        Connection connection = null;

        try {
            connection = Conexao.getConnection();

            String query = "SELECT * FROM address a inner join city c on a.city_ID = c.city_ID ; ";

            Statement st = connection.createStatement();

            ResultSet rs = st.executeQuery(query);

            while (rs.next()) {

                Cidade cidade = new Cidade(rs.getInt("city_ID"), rs.getString("city"));

                Endereco endereco = new Endereco(rs.getInt("address_ID"), rs.getString("address"), cidade);

                lista.add(endereco);
            }

        } catch (SQLException ex) {
            System.err.println("Erro ao atualizar ....");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar o banco");
            }
        }

        return lista;
    }

    @Override
    public Endereco buscarPorId(int id) {

        Connection connection = null;
        Endereco endereco = null ; 

        try {
            connection = Conexao.getConnection();

            String query = "SELECT * FROM address a inner join city c "
                    + "on a.city_ID = c.city_ID where address_ID = ?  ; ";

            PreparedStatement ps = connection.prepareStatement(query);

            ResultSet rs = ps.executeQuery();

            if (rs.first()) {
               
                Cidade cidade = new Cidade(rs.getInt("city_ID"), rs.getString("city"));

                endereco = new Endereco(rs.getInt("address_ID"), rs.getString("address"), cidade);
            }  
            

        } catch (SQLException ex) {
            System.err.println("Erro ao atualizar ....");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar o banco");
            }
        }
        
        return endereco;

    }
}
