
package br.com.senac.banco;

import java.util.List;

public interface DAO<T> {
    
    void salvar(T objeto);
    void atualizar(T objeto) ; 
    void deletar(int id) ; 
    List<T> listarTodos () ; 
    T buscarPorId(int id) ; 
       
}
