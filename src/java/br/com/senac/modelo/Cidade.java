
package br.com.senac.modelo;

public class Cidade {
    
    private int codigo;
    private String nome;
    private Pais pais;

    public Cidade() {
    }

    public Cidade(int codigo, String nome, Pais pais) {
        this.codigo = codigo;
        this.nome = nome;
        this.pais = pais;
    }

    public Cidade(int aInt, String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }
    
}
